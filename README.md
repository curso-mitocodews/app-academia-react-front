# Aplicación Academia Demo
_Este proyecto es una aplicación demostrativa de un registro de matrpicula para el curso SPRING WEBFLUX - REACT - mitocode._


## Objetivo
_Este proyecto tiene como finalidad aplicar lo aprendido en el curso, mediante la implementación de servicos REST (Anotaciones y Funcionales) para la parte de back end y el uso de React para el front end_


## Repositorio Público
  
* [Backend](https://bitbucket.org/curso-mitocodews/app-academia-react-back)
* [Frontend](https://bitbucket.org/curso-mitocodews/app-academia-react-front)


## Ejecutar el proyecto
_Para poder ejecutar el se debe compilar el proyecto backend en localhost:8080 y ejecutar para levantar el front ejecutar npm install y luego npm start._

_Fases:_

* Build
* Testing
* Sonar 
* Artifactory 
* Despliegue en Jboss 7.2

  

## Herramientas utilizadas

* [Java11]
* [EclipseSTS](
* [MongoDB4.2.x]
* [Robo3T]
* [Node.js]
* [Visual_Studio_Code]
* [JWT]
* [Spring2.3.3]
* [React]
* [cloudinary]
* [lombok]



## Autor
* **Gilmar Lam** - *Alumno* - [gilmarlam](https://bitbucket.org/gilmarlam/) 
 
 
