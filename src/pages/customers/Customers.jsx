import React, { useEffect, useState } from 'react'
import { Segment, Breadcrumb, Table, Divider, Header, Icon, Popup, Button, Container, Grid } from 'semantic-ui-react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { toast } from 'react-toastify'
import { openModal, closeModal } from '../../app/store/actions/modalActions'
import LoadingComponent from '../../components/common/LoadingComponent'
import CustomerForm from '../../components/customers/CustomerForm'
import CustomerService from '../../app/api/customerService'
import CustomerProfile from '../../components/customers/CustomerProfile'
import useFetchCustomers from '../../app/hooks/useFetchCustomers'

const actions = {
  openModal,
  closeModal,
}

const Customers = ({ openModal, closeModal }) => {
  const [customersList, setCustomersList] = useState([])
  const [loadingAction, setLoadingAction] = useState(false)
  const [loading, setLoading] = useState(true)

  const [customers] = useFetchCustomers()

  useEffect(() => {
    setLoading(true)
    if (customers) {
      setCustomersList(customers)
      setLoading(false)
    }
  }, [customers])

  const handleCreateorEdit = async (values) => {
    const customersUpdatedList = [...customersList]
    try {
      if (values.id) {
        const updatedCustomer = await CustomerService.updateCustomer(values)
        const index = customersUpdatedList.findIndex((a) => a.id === values.id)
        customersUpdatedList[index] = updatedCustomer
        toast.info('The student was updated')
      } else {
        const customer = {
          nombres: values.nombres,
          apellidos: values.apellidos,
          dni: values.dni,
          edad: values.edad,
          urlFoto: '',
        }
        const newCustomer = await CustomerService.addCustomer(customer)
        customersUpdatedList.push(newCustomer)
        toast.success('The Student was created')
      }
      setCustomersList(customersUpdatedList)
    } catch (error) {
      toast.error(error)
    }
    closeModal()
  }

  const handleDeleteCustomer = async (id) => {
    setLoadingAction(true)
    try {
      let customersUpdatedList = [...customersList]
      await CustomerService.removeCustomer(id)
      customersUpdatedList = customersUpdatedList.filter((a) => a.id !== id)
      setCustomersList(customersUpdatedList)
      setLoadingAction(false)
      toast.info('The Student was removed')
    } catch (error) {
      setLoadingAction(false)
      toast.error(error)
    }
  }

  const handleOrderStudent = async (id) => {

    setLoading(true)
    if (customers) {
      const listaOrdenada = await CustomerService.fetchOrderCustomers()
      setCustomersList(listaOrdenada)
      setLoading(false)
      toast.info('the list has been sorted')
    }
  }

  let customersArea = <h4>No existen categorias asociadas</h4>
  if (customersList && customersList.length > 0) {
    customersArea = (
      <Table>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell width="3">Name</Table.HeaderCell>
            <Table.HeaderCell width="2">Last Name</Table.HeaderCell>
            <Table.HeaderCell width="2">DNI</Table.HeaderCell>
            <Table.HeaderCell width="2">Age</Table.HeaderCell>
            <Table.HeaderCell width="3" />
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {customersList.map((customer) => (
            <Table.Row key={customer.id}>
              <Table.Cell>{customer.nombres}</Table.Cell>
              <Table.Cell>{customer.apellidos}</Table.Cell>
              <Table.Cell>{customer.dni}</Table.Cell>
              <Table.Cell>{customer.edad}</Table.Cell>
              <Table.Cell>
                <Popup
                  inverted
                  content="Update Student"
                  trigger={
                    <Button
                      basic
                      color="teal"
                      icon="edit"
                      loading={loadingAction}
                      onClick={() => {
                        openModal(<CustomerForm customerId={customer.id} submitHandler={handleCreateorEdit} />)
                      }}
                    />
                  }
                />
                <Popup
                  inverted
                  content="Delete Student"
                  trigger={
                    <Button
                     basic
                      color="red"
                      icon="trash"
                      loading={loadingAction}
                      onClick={() => {
                        handleDeleteCustomer(customer.id)
                      }}
                    />
                  }
                />
                <Popup
                  inverted
                  content="Upload Photo"
                  trigger={
                    <Button
                    basic
                      color="vk"
                      icon="cloud upload"
                      loading={loadingAction}
                      onClick={() => {
                        openModal(<CustomerProfile customerId={customer.id} />, 'large', true)
                      }}
                    />
                  }
                />
              </Table.Cell>
            </Table.Row>
          ))}
        </Table.Body>
      </Table>
    )
  }

  if (loading) return <LoadingComponent content="Loading Customers..." />

  return (
    <>
      <Segment>
        <Breadcrumb size="small">
        <Breadcrumb.Section>Resources Academy</Breadcrumb.Section>
          <Breadcrumb.Divider icon="right chevron" />
          <Breadcrumb.Section active>Students</Breadcrumb.Section>
        </Breadcrumb>
        <Divider horizontal>
          <Header as="h4">
            <Icon name="list alternate outline" />
            Students 
          </Header>
        </Divider>
        <Segment>
          <Button
            size="large"
            content="New Student"
            icon="add user"
            color="green"
            onClick={() => {
              openModal(<CustomerForm submitHandler={handleCreateorEdit} />)
            }}
          />
          <Button
            size="large"
            content="Order Student"
            icon="sort content ascending"
            color="teal"
            labelPosition='right'
            onClick={handleOrderStudent}
          />
        </Segment>
        <Container textAlign="center">
          <Grid columns="3">
            <Grid.Column width="3" />
            <Grid.Column width="10">{customersArea}</Grid.Column>
            <Grid.Column width="3" />
          </Grid>
        </Container> 
      </Segment>
    </>
  )
}

Customers.propTypes = {
  openModal: PropTypes.func.isRequired,
  closeModal: PropTypes.func.isRequired,
}

export default connect(null, actions)(Customers)
