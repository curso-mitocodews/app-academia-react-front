import React, { useCallback, useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import { toast } from 'react-toastify'
import PropTypes from 'prop-types'
import { Breadcrumb, Container, Divider, Grid, Header, Icon, Label, Segment, Table } from 'semantic-ui-react'

import CustomerService from '../../app/api/customerService'
import CourseService from '../../app/api/courseService'
import InvoiceService from '../../app/api/invoiceService'
import LoadingComponent from '../../components/common/LoadingComponent'

const InvoiceDetails = ({ match }) => {
  const [invoice, setInvoice] = useState(null)
  const [loading, setLoading] = useState(false)

  const fetchInvoice = useCallback(async () => {
    setLoading(true)
    try {
      const invoice = await InvoiceService.fetchInvoice(match.params.id)
      if (invoice) {
        const customer = await CustomerService.fetchCustomer(invoice.estudiante.id)

        const items = []
        if (invoice.cursos.length > 0) {
          invoice.cursos.forEach((item) => {
            CourseService.fetchCourse(item.id)
              .then((response) => {
                if (response) {
                  const dishItem = {
                    nombre: response.nombre,
                    siglas: response.siglas,
                    id: response.id,
                  }
                  items.push(dishItem)
                }

                const invoiceDetail = {
                  id: invoice.id,
                  estudiante: customer,
                  items: items,
                  fechaMatricula: invoice.fechaMatricula,
                }
                setInvoice(invoiceDetail)
              })
              .catch((error) => toast.error(error))
          })
        }
      }
      setLoading(false)
    } catch (error) {
      setLoading(false)
      toast.error(error)
    }
  }, [match.params.id])

  useEffect(() => {
    fetchInvoice()
  }, [fetchInvoice])

  if (loading) return <LoadingComponent content="Loading matrícula Details..." />
  let invoiceDetailedArea = <h4>Details</h4>

  if (invoice) {
    invoiceDetailedArea = (
      <Segment.Group>
        <Segment>
          <Header as="h4" block color="black">
            Student
          </Header>
        </Segment>
        <Segment.Group>
          <Segment>
            <p>
              <strong>Name: </strong>
              {`${invoice.estudiante.nombres} ${invoice.estudiante.apellidos}`}
            </p>
          </Segment>
        </Segment.Group>
        <Segment>
          <Header as="h4" block color="black">
            Matricula
          </Header>
        </Segment>
        <Segment.Group>
          <Segment>
            <p>
              <strong>Code: </strong>
              {invoice.id}
            </p>
            <p>
              <strong>Date: </strong>
              {invoice.fechaMatricula}
            </p>
          </Segment>
        </Segment.Group>
        <Segment>
          <Table celled striped color="black">
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell colSpan="4">
                  <Icon name="book" />
                  Courses
                </Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell>Name</Table.HeaderCell>
                <Table.HeaderCell>Acronym</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>
              {invoice.items.length > 0 &&
                invoice.items.map((item) => (
                  <Table.Row key={item.id}>
                    <Table.Cell>{item.nombre}</Table.Cell>
                    <Table.Cell>{item.siglas}</Table.Cell>
                  </Table.Row>
                ))}
            </Table.Body>
          </Table>
          <Container textAlign="right">
            <Label basic color="black" size="large">
            Total Courses:
              <Label.Detail content={`${+invoice.items.length}`} />
            </Label>
          </Container>
        </Segment>
      </Segment.Group>
    )
  }

  return (
    <Segment>
      <Breadcrumb size="small">
        <Breadcrumb.Section>Matricula</Breadcrumb.Section>
        <Breadcrumb.Divider icon="right chevron" />
        <Breadcrumb.Section as={Link} to="/matriculas">
          List
        </Breadcrumb.Section>
        <Breadcrumb.Divider icon="right chevron" />
        <Breadcrumb.Section active>Detail</Breadcrumb.Section>
      </Breadcrumb>
      <Divider horizontal>
        <Header as="h4">
          <Icon name="address card outline" />
          Detail of Matricula
        </Header>
      </Divider>
      <Container>
        <Grid columns="3">
          <Grid.Column width="3" />
          <Grid.Column width="10">{invoiceDetailedArea}</Grid.Column>
          <Grid.Column width="3" />
        </Grid>
      </Container>
    </Segment>
  )
}

InvoiceDetails.propTypes = {
  match: PropTypes.object.isRequired,
}

export default InvoiceDetails
