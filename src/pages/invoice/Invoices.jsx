import React, { useCallback, useEffect, useState } from 'react'
import { toast } from 'react-toastify'
import { Breadcrumb, Button, Container, Divider, Grid, Header, Icon, Popup, Segment, Table } from 'semantic-ui-react'
import PropTypes from 'prop-types'

import InvoiceService from '../../app/api/invoiceService'
import StudentService from '../../app/api/studentService'
import LoadingComponent from '../../components/common/LoadingComponent'

const Invoices = ({ history }) => {
  const [invoices, setInvoices] = useState([])
  const [loading, setLoading] = useState(false)

  const fetchInvoices = useCallback(async () => {
    setLoading(true)
    try {
      const invoices = await InvoiceService.fetchInvoices()
      
      const matriculas = []
      if(invoices){
        invoices.forEach( (m) => {
           StudentService.fetchStudent(m.estudiante.id)
          .then((response) => {
            if (response) {
              const estudianteItem = {
                nombres: response.nombres,
                apellidos: response.apellidos,
                id: response.id
              }
              console.log("estudianteItem "+JSON.stringify(estudianteItem))
              const matricula = {
                id: m.id,
                estudiante: estudianteItem,
                cursos: null,
                fechaMatricula: m.fechaMatricula,
              }
              console.log("matricula "+JSON.stringify(matricula))
              matriculas.push(matricula)
            }
          }).catch((error) => toast.error(error))
        })

        const timer = setTimeout(() => {
          setInvoices(matriculas)
          setLoading(false)
        }, 1000);
        return () => clearTimeout(timer);
        
      }
    } catch (error) {
      setLoading(false)
      toast.error(error)
    }
  }, [])

  useEffect(() => {
    fetchInvoices()
  }, [fetchInvoices])

  let invoicesList = <h4>No hay matrículas registradas</h4>

  if (invoices && invoices.length > 0) {
    invoicesList = (
      <Table>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell width="5">Student</Table.HeaderCell>
            <Table.HeaderCell width="2">Date of Matrícula</Table.HeaderCell>
            <Table.HeaderCell width="3" />
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {invoices.map((invoice) => (
            <Table.Row key={invoice.id}>
              <Table.Cell>{invoice.estudiante.nombres} {invoice.estudiante.apellidos}</Table.Cell>
              <Table.Cell>{new Date(invoice.fechaMatricula).toLocaleDateString()}</Table.Cell>
              <Table.Cell>
                <Popup
                  inverted
                  content="Invoice Detail"
                  trigger={
                    <Button
                      color="teal"
                      icon="address card outline"
                      onClick={() => {
                        history.push(`/matricula/${invoice.id}`)
                      }}
                    />
                  }
                />
              </Table.Cell>
            </Table.Row>
          ))}
        </Table.Body>
      </Table>
    )
  }

  if (loading) return <LoadingComponent content="Loading matrícula..." />

  return (
    <Segment>
      <Breadcrumb size="small">
        <Breadcrumb.Section>Matricula</Breadcrumb.Section>
        <Breadcrumb.Divider icon="right chevron" />
        <Breadcrumb.Section active>List</Breadcrumb.Section>
      </Breadcrumb>
      <Divider horizontal>
        <Header as="h4">
          <Icon name="list alternate outline" />
          Matrículas
        </Header>
      </Divider>
      <Container>
        <Grid columns="3">
          <Grid.Column width="3" />
          <Grid.Column width="10">{invoicesList}</Grid.Column>
          <Grid.Column width="3" />
        </Grid>
      </Container>
    </Segment>
  )
}

Invoices.propTypes = {
  history: PropTypes.object.isRequired,
}

export default Invoices
