// keys
export const TOKEN_KEY = 'token'

// endpoints
// export const CUSTOMER_ENDPOINT = '/clientes'
export const AUTH_ENDPOINT = '/login'
export const DISHES_ENDPOINT = '/cursos'
 
export const INVOICE_ENDPOINT = '/matriculas' 
export const STUDENT_ENDPOINT = '/estudiantes' 
export const COURSE_ENDPOINT = '/cursos'