import { STUDENT_ENDPOINT } from '../core/appConstants'
import baseApi from './baseApi'

const getCustomerUrl = (id) => `${STUDENT_ENDPOINT}/${id}`

class CustomerService {
    static fetchCustomers = () => baseApi.get(STUDENT_ENDPOINT)

    static fetchOrderCustomers = () => baseApi.get(`${STUDENT_ENDPOINT}/ordenByDesc`)

    static fetchCustomer = (id) => baseApi.get(getCustomerUrl(id))

    static addCustomer = (customer) => baseApi.post(STUDENT_ENDPOINT, customer)

    static updateCustomer = (customer) => baseApi.put(STUDENT_ENDPOINT, customer)

    static uploadCustomerPhoto = (id, photo) => baseApi.postForm(`${STUDENT_ENDPOINT}/subir/cloud/${id}`, photo)

    static removeCustomer = (id) => baseApi.delete(getCustomerUrl(id))
}

export default CustomerService