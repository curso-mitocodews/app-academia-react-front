import baseApi from './baseApi'
import { DISHES_ENDPOINT } from '../core/appConstants'

const getDishUrl = (id) => `${DISHES_ENDPOINT}/${id}`

class DishService {
  static fetchDishes = () => baseApi.get(DISHES_ENDPOINT)

  static fetchDish = async (id) => baseApi.get(getDishUrl(id))

  static addDish = async (dish) => baseApi.post(DISHES_ENDPOINT, dish)

  static updateDish = async (dish) => baseApi.put(getDishUrl(dish.id), dish)

  static deleteDish = async (id) => baseApi.delete(getDishUrl(id))
}

export default DishService
