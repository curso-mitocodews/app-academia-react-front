import { combineReducers } from 'redux'
import authReducer from './authReducer'
import dishReducer from './dishReducer'
import courseReducer from './courseReducer'
import modalReducer from './modalReducer'

const rootReducer = combineReducers({
    // set global state
    modal: modalReducer,
    auth: authReducer,
    dish: dishReducer,
    course: courseReducer,
})

export default rootReducer