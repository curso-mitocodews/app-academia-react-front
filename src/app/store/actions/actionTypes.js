// Modal
export const MODAL_OPEN = 'MODAL_OPEN'
export const MODAL_CLOSE = 'MODAL_CLOSE'

// Auth
export const LOGIN_USER = 'LOGIN_USER'
export const LOGOUT_USER = 'LOGOUT_USER'
export const CURRENT_USER = 'CURRENT_USER'

// Dish
export const LOADING_DISHES = 'LOADING_DISHES'
export const LOADING_DISH = 'LOADING_DISH'
export const FETCH_DISHES = 'FETCH_DISHES'
export const FETCH_DISH = 'FETCH_DISH'
export const ADD_DISH = 'ADD_DISH'
export const UPDATE_DISH = 'UPDATE_DISH'
export const DELETE_DISH = 'DELETE_DISH'


// Course
export const LOADING_COURSES = 'LOADING_COURSES'
export const LOADING_COURSE = 'LOADING_COURSE'
export const FETCH_COURSES = 'FETCH_COURSES'
export const FETCH_COURSE = 'FETCH_COURSE'
export const ADD_COURSE = 'ADD_COURSE'
export const UPDATE_COURSE = 'UPDATE_COURSE'
export const DELETE_COURSE = 'DELETE_COURSE'