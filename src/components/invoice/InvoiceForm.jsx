import React, { useEffect, useState } from 'react'
import { Form as FinalForm, Field } from 'react-final-form'
import { combineValidators, isRequired } from 'revalidate'
import { Button, Form, Grid, Header, Popup, Table } from 'semantic-ui-react'
import { connect } from 'react-redux'
import { toast } from 'react-toastify'
import PropTypes from 'prop-types'
import history from '../..'
import ErrorMessage from '../form/ErrorMessage'
import TextAreaInput from '../form/TextAreaInput'
import SelectedInput from '../form/SelectedInput'

import { fetchCourses } from '../../app/store/actions/courseActions'
import InvoiceService from '../../app/api/invoiceService'
import useFetchCustomers from '../../app/hooks/useFetchCustomers'

const validate = combineValidators({
  // descripcion: isRequired({ message: 'The description is required' }),
  // observacion: isRequired({ message: 'The observation is required' }),
  estudiante: isRequired(''),
  curso: isRequired(''),
})

const actions = {
  fetchCourses,
}

const mapState = (state) => {
  const courses = []

  if (state.course.courses && state.course.courses.length > 0) {
    state.course.courses.forEach((item) => {
      const course = {
        key: item.id,
        text: item.nombre,
        siglas: item.siglas,
        value: item.id,
      }
      courses.push(course)
    })
  }

  return {
    loading: state.course.loadingCourses,
    courses,
  }
}

const InvoiceForm = ({ fetchCourses, courses, loading }) => {
  const [customers] = useFetchCustomers()
  const [customersList, setCustomersList] = useState([])
  const [loadingCustomers, setLoadingCustomers] = useState(true)
  const [items, setItems] = useState([])
  const [item, setItem] = useState(null)

  useEffect(() => {
    if (courses.length === 0) {
      fetchCourses()
    }
    setLoadingCustomers(true)
    if (customers) {
      const customersList = []
      customers.forEach((item) => {
        const customer = {
          key: item.id,
          text: `${item.nombres} ${item.apellidos}`,
          value: item.id,
        }
        customersList.push(customer)
      })
      setCustomersList(customersList)
      setLoadingCustomers(false)
    }
  }, [customers, courses.length, fetchCourses])

  const handleAddingItems = () => {
    const newItems = [...items]
    const coursesList = [...courses]
    const index = newItems.findIndex((a) => a.id === item)
    if (index > -1) {
      newItems[index] = {
        id: newItems[index].id,
        name: newItems[index].name,
        siglas: newItems[index].siglas,
        // quantity: newItems[index].quantity + 1,
      }
      setItems(newItems)
    } else {
      const newItem = {
        id: item,
        name: coursesList.filter((a) => a.key === item)[0].text,
        siglas: coursesList.filter((a) => a.key === item)[0].siglas,
      }
      newItems.push(newItem)
    }
    setItems(newItems)
  }

  const handleRemoveItems = (id) => {
    let updatedItems = [...items]
    updatedItems = updatedItems.filter((a) => a.id !== id)
    setItems(updatedItems)
  }

  const handleAddNewInvoice = async (values) => {
    const itemsForInvoice = [...items]
    // const itemsForInvoice = newItems.map((item) => {
    //   return { course: { id: item.id } }
    // })

    const newInvoice = {
      estudiante: {
        id: values.estudiante,
      },
      cursos: itemsForInvoice,
      estado: true,
    }
    try {
      const invoice = await InvoiceService.createInvoice(newInvoice)
      toast.info('The matrícula was sucessfully registered')
      history.push(`matricula/${invoice.id}`)
    } catch (error) {
      toast.error(error)
    }
  }

  return (
    <FinalForm
      onSubmit={(values) => handleAddNewInvoice(values)}
      validate={validate}
      render={({ handleSubmit, submitting, submitError, invalid, pristine, dirtySinceLastSubmit }) => (
        <Form onSubmit={handleSubmit} error loading={loading || loadingCustomers}>
          <Header as="h2" content="Add New Matricula" color="black" textAlign="center" />
          <Field
            name="estudiante"
            component={SelectedInput}
            placeholder="Select a student"
            options={customersList}
            width="6"
          />
          <Grid columns="6">
            <Grid.Row columns="2">
              <Grid.Column width="5">
                <Field
                  name="curso"
                  component={SelectedInput}
                  placeholder="Select a course"
                  options={courses}
                  width="3"
                  handleOnChange={(e) => setItem(e)}
                />
              </Grid.Column>
              <Grid.Column>
                <Popup
                  inverted
                  content="Add"
                  trigger={
                    <Button
                      type="button"
                      loading={submitting}
                      color="blue"
                      icon="plus circle"
                      onClick={handleAddingItems}
                      disabled={!item}
                    />
                  }
                />
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              {items && items.length > 0 && (
                <Table celled collapsing style={{ marginLeft: '2%' }}>
                  <Table.Header>
                    <Table.Row>
                      <Table.HeaderCell>Course</Table.HeaderCell>
                      <Table.HeaderCell>Acronym</Table.HeaderCell>
                      <Table.HeaderCell />
                    </Table.Row>
                  </Table.Header>
                  <Table.Body>
                    {items.map((item) => (
                      <Table.Row key={item.id}>
                        <Table.Cell>{item.name}</Table.Cell>
                        <Table.Cell>{item.siglas}</Table.Cell>
                        {/* <Table.Cell textAlign="center">{item.quantity}</Table.Cell> */}
                        <Table.Cell>
                          <Popup
                            inverted
                            content="Delete"
                            trigger={
                              <Button
                              basic
                                color="red"
                                icon="remove circle"
                                type="button"
                                onClick={() => handleRemoveItems(item.id)}
                              />
                            }
                          />
                        </Table.Cell>
                      </Table.Row>
                    ))}
                  </Table.Body>
                </Table>
              )}
            </Grid.Row>
          </Grid>
          <br />
          {submitError && !dirtySinceLastSubmit && <ErrorMessage error={submitError} text="Invalid Values" />}
          <Button
            fluid
            disabled={(invalid && !dirtySinceLastSubmit) || pristine || items.length === 0}
            loading={submitting}
            color="teal"
            content="Save"
          />
        </Form>
      )}
    />
  )
}

InvoiceForm.propTypes = {
  fetchCourses: PropTypes.func.isRequired,
  Courses: PropTypes.array.isRequired,
  loading: PropTypes.bool.isRequired,
}

export default connect(mapState, actions)(InvoiceForm)
