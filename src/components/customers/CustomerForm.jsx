import React, { useEffect, useState } from 'react'
import { combineValidators, isRequired } from 'revalidate'
import { Form as FinalForm, Field } from 'react-final-form'
import { Button, Form, Header } from 'semantic-ui-react'
import PropTypes from 'prop-types'
import useFetchCustomer from '../../app/hooks/useFetchCustomer'
import ErrorMessage from '../form/ErrorMessage'
import TextInput from '../form/TextInput'

const validate = combineValidators({
  nombres: isRequired({ message: 'The name is required' }),
  apellidos: isRequired({ message: 'The lastName is required' }),
  dni: isRequired({ message: 'The dni is required' }),
  edad: isRequired({ message: 'The age is required' }),
})

const CustomerForm = ({ customerId, submitHandler }) => {
  const [actionLabel, setActionLabel] = useState('Add Student')
  // Custom hook
  const [customer, loading] = useFetchCustomer(customerId)

  useEffect(() => {
    if (customerId) {
      setActionLabel('Edit Student')
    } else setActionLabel('Add Student')
  }, [customerId])

  return (
    <FinalForm
      onSubmit={(values) => submitHandler(values)}
      initialValues={customerId && customer}
      validate={validate}
      render={({ handleSubmit, submitting, submitError, invalid, pristine, dirtySinceLastSubmit }) => (
        <Form onSubmit={handleSubmit} error loading={loading}>
          <Header as="h2" content={actionLabel} color="teal" textAlign="center" />
          <Field name="nombres" component={TextInput} placeholder="Type the Name" />
          <Field name="apellidos" component={TextInput} placeholder="Type the Lastname" />
          <Field name="dni" component={TextInput} placeholder="Type the dni" />
          <Field name="edad" component={TextInput} type="number" placeholder="Type the age" />
          {submitError && !dirtySinceLastSubmit && (
            <ErrorMessage error={submitError} text="Invalid username or password" />
          )}
          <Button
            fluid
            disabled={(invalid && !dirtySinceLastSubmit) || pristine}
            loading={submitting}
            color="teal"
            content={actionLabel}
          />
        </Form>
      )}
    />
  )
}

CustomerForm.propTypes = {
  customerId: PropTypes.string,
  submitHandler: PropTypes.func.isRequired,
}

CustomerForm.defaultProps = {
  customerId: null,
}

export default CustomerForm
