import React, { useEffect, useState } from 'react'
import { Form, Header, Button } from 'semantic-ui-react'
import { Form as FinalForm, Field } from 'react-final-form'
import { combineValidators, composeValidators, isRequired } from 'revalidate'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import TextInput from '../../form/TextInput'
import { fetchCourse, addCourse, updateCourse } from '../../../app/store/actions/courseActions'
import ErrorMessage from '../../form/ErrorMessage'

const validate = combineValidators({
  nombre: isRequired({ message: 'Please type a name' }),
  siglas: composeValidators(isRequired({ message: 'The siglas is required' }))(),
})

const actions = {
  fetchCourse,
  addCourse,
  updateCourse,
}

const mapState = (state) => ({
  course: state.course.course,
  loading: state.course.loadingCourse,
})

const CoursesForm = ({ id, course, fetchCourse, loading, addCourse, updateCourse }) => {
  const [actionLabel, setActionLabel] = useState('Add Course')

  useEffect(() => {
    if (id) {
      fetchCourse(id)
      setActionLabel('Edit Course')
    } else setActionLabel('Add Course')
  }, [fetchCourse, id])

  const handleCreateorEdit = (values) => {
    if (id) {
      updateCourse(values)
    } else {
      const newCourse = {
        nombre: values.nombre,
        siglas: values.siglas,
        estado: true,
      }
      addCourse(newCourse)
    }
  }

  return (
    <FinalForm
      onSubmit={(values) => handleCreateorEdit(values)}
      initialValues={id && course}
      validate={validate}
      render={({ handleSubmit, submitting, submitError, invalid, pristine, dirtySinceLastSubmit }) => (
        <Form onSubmit={handleSubmit} error loading={loading}>
          <Header as="h2" content={actionLabel} color="teal" textAlign="center" />
          <Field name="nombre" component={TextInput} placeholder="Type the Course name" />
          <Field name="siglas" component={TextInput} placeholder="Type the siglas of the Course" />
          {submitError && !dirtySinceLastSubmit && <ErrorMessage error={submitError} text="Invalid values" />}
          <Button
            fluid
            disabled={(invalid && !dirtySinceLastSubmit) || pristine}
            loading={submitting}
            color="teal"
            content={actionLabel}
          />
        </Form>
      )}
    />
  )
}

CoursesForm.propTypes = {
  id: PropTypes.string,
  course: PropTypes.object,
  fetchCourse: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
  addCourse: PropTypes.func.isRequired,
  updateCourse: PropTypes.func.isRequired,
}

CoursesForm.defaultProps = {
  id: null,
  course: null,
}

export default connect(mapState, actions)(CoursesForm)
